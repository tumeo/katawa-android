# Katawa Shoujo Android

	Source: Katawa Shoujo 1.3.1
	Languages: English, Français, Español Internacional, 日本語.
	Android-Build: 2

	Lite version: Lossless compression images.
	Videos: Working (VP8/Vorbis).
	Fixed: Bug in the ending credits.
	Fixed: Game does not stop without the "menu button".
	Added: A touch button to pause the game.

## Download

Dropbox: https://link.tumeo.space/katawa-android-131-2